
provider "aws" {
  region  = "eu-west-1"
}

resource "aws_key_pair" "deployer" {
  key_name = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCh7U/oFBtaJbEAqDQFRC3fP2RWzb74MppQcbots+WiNsfRNqbGfX6RAqxcZSyc0/4HgKjXyAuSMdRwr5mrwWVxmHX4ucNlbMHFoth2VLiJXxmnCFHIOjfq/c67C0D2LWAaNWZUWMxnJwLxiXegojmjip8Lrb/GlXx3jOVx1hNnwUGHDtwH3XQ0PKbR4rG59wvXMBTZl1dsuMpD7nCXcxiXMNqak1QN+tS5QCu0HG95LPw2lyYueXbjr9yVvWck5i9Z23DZgsH+ncQ2nbvaxA7l2gpXUGBlN0BOCV+Ib2MXVs2nCJNrkUaQ+MMpZaGQq02VAEzycKdARbNsDnBwpwDHdTRQIl+S63Vmj1HoJV2UDskeBkZ4bowCLoeu7D7M2wUwuOBrVIBo1QLq6xSmKRK8xNyYl23KPlu7qq4pXwZhKnTMqhKoBoqc0WedOtBRhGOLFL33FQYHvKu6NrfLCRqNxHrpoI1P7Xvv5C0xa9uawJO9/WygA3SjCcRCnRt10HM= root@ubuntu"

}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_default_vpc.default.id

  ingress {
    protocol  = "tcp"
    self      = true
    from_port = 80
    to_port   = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    cidr_blocks = [
      "0.0.0.0/0"
    ]
    from_port = 22
    to_port = 22
    protocol = "tcp"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "web" {
  ami           = var.ami_id
  key_name = aws_key_pair.deployer.key_name

  instance_type = "t2.micro"

  tags = {
    Name = "HelloWorld"
  }
}
/***
resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "GameScores"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "UserId"
  range_key      = "GameTitle"

  attribute {
    name = "UserId"
    type = "S"
  }

  attribute {
    name = "GameTitle"
    type = "S"
  }

  attribute {
    name = "TopScore"
    type = "N"
  }

  ttl {
    attribute_name = "TimeToExist"
    enabled        = false
  }

  global_secondary_index {
    name               = "GameTitleIndex"
    hash_key           = "GameTitle"
    range_key          = "TopScore"
    write_capacity     = 10
    read_capacity      = 10
    projection_type    = "INCLUDE"
    non_key_attributes = ["UserId"]
  }

  tags = {
    Name        = "dynamodb-table-1"
    Environment = "production"
  }
}

resource "aws_iam_role" "role" {
  name = "test-role"

  assume_role_policy = <<EOF
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Action": "sts:AssumeRole",
          "Principal": {
            "Service": "ec2.amazonaws.com"
          },
          "Effect": "Allow",
          "Sid": ""
        }
      ]
    }
EOF

}

resource "aws_iam_policy" "policy" {
  name        = "test-policy"
  description = "A test policy"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "dynamodb:*",
            "Resource": "*"
        }
    ]
}
EOF

}

resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.role.name
  policy_arn = aws_iam_policy.policy.arn
}
**/
